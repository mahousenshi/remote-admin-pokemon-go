from flask import Blueprint, render_template, request, redirect, url_for, flash
from datetime import datetime
from itertools import combinations

import sqlite3

tournament = Blueprint('tournament', __name__)
tournament.secret_key = '1799d13938a0960d6560faa5'

def dict_factory(cursor, row):
    return {col[0]: row[idx] for idx, col in enumerate(cursor.description)}

def open_db(name):
    conn = sqlite3.connect(name)
    conn.row_factory = dict_factory

    c = conn.cursor()

    return conn, c

def close_db(conn, cursor):
    cursor.close()
    conn.close()

# =========== #
# Tournaments #
@tournament.route('/tournaments/')
def tournaments():
    conn, c = open_db('remote.db')

    sql = '''
    SELECT *
      FROM tournaments
    '''

    c.execute(sql)

    tournaments = c.fetchall()

    open_tournaments   = list(filter(lambda x: x['tournament_is_closed'] == 0, tournaments))
    closed_tournaments = list(filter(lambda x: x['tournament_is_closed'] == 1, tournaments))

    sql = '''
    SELECT trainer_id, trainer_nick
      FROM trainers
    '''

    c.execute(sql)

    trainers = c.fetchall()

    close_db(conn, c)

    return render_template('tournaments.html', open_tournaments = open_tournaments, closed_tournaments = closed_tournaments, trainers = trainers)

@tournament.route('/tournament/add')
def tournament_add():
    name = request.args.get('name')
    trainers = sorted(map(int, request.args.getlist('trainers')))

    if len(name) < 4 or len(name) > 15:
        flash('Invalid tournament name (More than 3 less than 16)!', 'error')

    elif len(trainers) == 1:
        flash('Too few trainers!</p>', 'error')

    else:
        conn, c = open_db('remote.db')

        sql = '''
        INSERT INTO tournaments (tournament_name, tournament_URL, tournament_is_closed)
        VALUES (?, ?, ?)
        '''

        c.execute(sql, (name, '', 0))
        conn.commit()

        tournament = c.lastrowid

        for trainer in trainers:
            sql = '''
            INSERT INTO tournaments_trainers (trainer_id, tournament_id)
            VALUES (?, ?)
            '''

            c.execute(sql, (trainer, tournament))
            conn.commit()

        close_db(conn, c)

    return redirect(url_for('.tournaments'))

@tournament.route('/tournament/edit/<int:tournament_id>')
def tournament_edit(tournament_id):
    conn, c = open_db('remote.db')

    sql = '''
    SELECT tournament_id, tournament_name, tournament_is_closed
      FROM tournaments
     WHERE tournament_id = ?
    '''

    c.execute(sql, (tournament_id, ))

    tournament = c.fetchone()

    if tournament is None:
        flash('Invalid Tournament!', 'error')

        close_db(conn, c)

        return redirect(url_for('.tournaments'))
    else:
        if tournament['tournament_is_closed']:
            flash('Tournament is closed!', 'error')

            close_db(conn, c)

            return redirect(url_for('.tournaments'))
        else:
            sql = '''
            SELECT trainer_id
            FROM tournaments_trainers
            WHERE tournament_id = ?
            '''

            c.execute(sql, (tournament_id, ))

            tournament_trainers = sorted([trainer['trainer_id'] for trainer in c.fetchall()])

            sql = '''
            SELECT trainer_id, trainer_nick
            FROM trainers
            '''

            c.execute(sql)

            trainers = c.fetchall()

            close_db(conn, c)

            return render_template('tournament_edit.html', tournament = tournament, tournament_trainers = tournament_trainers, trainers = trainers)

@tournament.route('/tournament/edit')
def tournament_edit_action():
    tid = request.args.get('id')
    name = request.args.get('name')
    trainers_new = sorted(map(int, request.args.getlist('trainers')))

    close = 1
    if request.args.get('close') is None:
        close = 0

    if len(name) < 4 or len(name) > 15:
        flash('Invalid tournament name (More than 3 less than 16)!', 'error')

    elif len(trainers_new) == 1:
        flash('Too few trainers!</p>', 'error')

    else:
        conn, c = open_db('remote.db')

        sql = '''
        UPDATE tournaments
          SET tournament_name = ?,
              tournament_is_closed = ?
        WHERE tournament_id = ?
        '''

        c.execute(sql, (name, close, tid))
        conn.commit()

        sql = '''
        SELECT trainer_id
          FROM tournaments_trainers
         WHERE tournament_id = ?
         ORDER BY trainer_id
        '''

        c.execute(sql, (tid, ))

        trainers_old = sorted([trainer['trainer_id'] for trainer in c.fetchall()])

        trainers_remove = list(set(trainers_old) - set(trainers_new))

        sql = '''
        DELETE FROM tournaments_trainers
         WHERE trainer_id in (?)
        '''

        c.execute(sql, (', '.join(map(str, trainers_remove)), ))
        conn.commit()

        trainers_add = sorted(list(set(trainers_new) - set(trainers_old)))

        for trainer in trainers_add:
            sql = '''
            INSERT INTO tournaments_trainers (trainer_id, tournament_id)
            VALUES (?, ?)
            '''

            c.execute(sql, (trainer, tid))
            conn.commit()

        close_db(conn, c)

    return redirect(url_for('.tournaments'))

@tournament.route('/tournament/remove/<int:tournament_id>')
def tournament_remove(tournament_id):
    conn, c = open_db('remote.db')

    sql = '''
    DELETE FROM tournaments
     WHERE tournament_id = ?
    '''

    c.execute(sql, (tournament_id, ))
    conn.commit()

    sql = '''
    DELETE FROM tournaments_trainers
     WHERE tournament_id = ?
    '''

    c.execute(sql, (tournament_id, ))
    conn.commit()

    close_db(conn, c)

    return redirect(url_for('.tournaments'))

@tournament.route('/tournament/friendships/<int:tournament_id>')
def tournament_friendships(tournament_id):
    conn, c = open_db('remote.db')

    sql = '''
    SELECT trainer_id
      FROM tournaments_trainers
     WHERE tournament_id = ?
    '''

    c.execute(sql, (tournament_id, ))

    tournaments_trainers = tuple([elem['trainer_id'] for elem in c.fetchall()])

    args = f'({", ".join("?" * len(tournaments_trainers))})'

    sql = f'''
    SELECT *
      FROM trainers
     WHERE trainer_id in {args}
    '''

    c.execute(sql, tournaments_trainers)

    trainers = []

    for trainer in c.fetchall():
        trainer['friendships'] = {}

        trainers.append(trainer)

    sql = f'''
    SELECT *
      FROM friendships
     WHERE trainer1_id in {args}
        OR trainer2_id in {args}
     ORDER BY trainer1_id, trainer2_id
    '''

    c.execute(sql, tournaments_trainers * 2)

    friendships = c.fetchall()

    close_db(conn, c)

    for trainer1, trainer2 in combinations(trainers, 2):
        trainer1

        friendship = next((elem for elem in friendships if (elem['trainer1_id'] == trainer1['trainer_id'] and elem['trainer2_id'] == trainer2['trainer_id']) or (elem['trainer1_id'] == trainer2['trainer_id'] and elem['trainer2_id'] == trainer1['trainer_id'])), None)

        if friendship is None:
            friendship = {
                'friendship_id': 0,
                'friendship_level': 'nofriendship ',
                'friendship_note': '',
            }

        else:
            if friendship['friendship_days'] < 1:
                friendship['friendship_level'] = 'best'
            elif 1 <= friendship['friendship_days'] <= 30:
                friendship['friendship_level'] = 'ultra'
            elif 30 < friendship['friendship_days'] <= 83:
                friendship['friendship_level'] = 'great'
            else:
                friendship['friendship_level'] = 'good'

            print(friendship['friendship_level'])

            today = datetime.now()
            last_interaction = datetime.fromtimestamp(friendship['friendship_last_interaction'])

            if today.date() == last_interaction.date():
                friendship['friendship_level'] += ' daily'

            friendship['friendship_note'] = ''

            if (trainer1['trainer_egg'] or trainer2['trainer_egg']) and friendship['friendship_days'] in (84, 30, 1):
                friendship['friendship_level'] += ' egg'
                friendship['friendship_note'] += '🥚'

            if friendship['friendship_luck']:
                friendship['friendship_note'] += '🌟'

            if friendship['friendship_days'] >= 30:
                delta = today - last_interaction

                friendship['friendship_note'] += f'{"⚠️" * (delta.days // 5)}'

        trainer1['friendships'][trainer2['trainer_id']] = friendship
        trainer2['friendships'][trainer1['trainer_id']] = friendship

    return render_template('tournaments_friendships.html', trainers = trainers)