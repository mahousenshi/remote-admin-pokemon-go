from flask import Blueprint, render_template, request, redirect, url_for, flash, jsonify
from datetime import datetime

#flash('URL para a leaderboard inválida!', 'error')

import sqlite3

friendship_table = Blueprint('friendship_table', __name__)
friendship_table.secret_key = 'c36f578df5afeba94de72c37'

def dict_factory(cursor, row):
    return {col[0]: row[idx] for idx, col in enumerate(cursor.description)}

def open_db(name):
    conn = sqlite3.connect(name)
    conn.row_factory = dict_factory

    c = conn.cursor()

    return conn, c

def close_db(conn, cursor):
    cursor.close()
    conn.close()

# ================ #
# Friendship Table #
@friendship_table.route('/_update')
def friendship_update():
    fid = request.args.get('id')

    conn, c = open_db('remote.db')

    sql = '''
    SELECT friendship_id, friendship_days, friendship_luck, friendship_last_interaction
      FROM friendships
     WHERE friendship_id == ?
    '''

    c.execute(sql, (fid, ))

    friendship = c.fetchone()

    today = datetime.now()
    last_interaction = datetime.fromtimestamp(friendship['friendship_last_interaction'])

    if not friendship is None and today.date() != last_interaction.date():
        friendship['friendship_last_interaction'] = datetime.now()

        friendship['friendship_note'] = ' '

        if friendship['friendship_days']:
            friendship['friendship_days'] -= 1

            if 1 == friendship['friendship_days'] <= 30:
                friendship['friendship_level'] = 'ultra daily'
            elif 30 < friendship['friendship_days'] <= 83:
                friendship['friendship_level'] = 'great daily'
            else:
                friendship['friendship_level'] = 'good daily'

        else:
            friendship['friendship_level'] = 'best'
            friendship['friendship_luck'] = (friendship['friendship_luck'] + 1) % 2

            if friendship['friendship_luck']:
                friendship['friendship_note'] += '🌟'

        sql = '''
        UPDATE friendships
          SET friendship_days = ?,
              friendship_luck = ?,
              friendship_last_interaction = ?
        WHERE friendship_id = ?
        '''

        c.execute(sql, (friendship['friendship_days'], friendship['friendship_luck'], int(datetime.timestamp(datetime.now())), fid))
        conn.commit()

    close_db(conn, c)

    return jsonify(friendship)

@friendship_table.route('/_new')
def friendship_new():
    trainers = [int(request.args.get('trainer1')), int(request.args.get('trainer2'))]

    if trainers[0] != trainers[1]:
        conn, c = open_db('remote.db')

        sql = '''
        SELECT friendship_id
          FROM friendships
         WHERE trainer1_id == ? AND trainer2_id == ?
        '''
        c.execute(sql, (min(trainers), max(trainers)))

        if c.fetchone() is None:
            sql = '''
            INSERT INTO friendships (trainer1_id, trainer2_id, friendship_days, friendship_luck, friendship_last_interaction)
            VALUES (?, ?, ?, ?, ?)
            '''

            c.execute(sql, (min(trainers), max(trainers), 89, 0, int(datetime.timestamp(datetime.now()))))
            conn.commit()

            friendship_id = c.lastrowid

        close_db(conn, c)

    return jsonify({'friendship_id': friendship_id})