from flask import Blueprint, render_template, request, redirect, url_for, flash
from datetime import datetime

import re

import sqlite3

friendship = Blueprint('friendship', __name__)
friendship.secret_key = 'c36f578df5afeba94de72c37'

def dict_factory(cursor, row):
    return {col[0]: row[idx] for idx, col in enumerate(cursor.description)}

def open_db(name):
    conn = sqlite3.connect(name)
    conn.row_factory = dict_factory

    c = conn.cursor()

    return conn, c

def close_db(conn, cursor):
    cursor.close()
    conn.close()

# =========== #
# Friendships #
@friendship.route('/friendships/')
def friendships():
    conn, c = open_db('remote.db')

    sql = '''
    SELECT trainer_id, trainer_nick
      FROM trainers
    '''

    c.execute(sql)

    trainers = c.fetchall()

    sql = '''
    SELECT friendships.friendship_id as friendship_id,
           trainers1.trainer_nick as trainer1_nick,
           trainers2.trainer_nick as trainer2_nick
      FROM friendships
           INNER JOIN trainers as trainers1
           ON trainers1.trainer_id = friendships.trainer1_id

           INNER JOIN trainers as trainers2
           ON trainers2.trainer_id = friendships.trainer2_id
    '''

    c.execute(sql)

    friendships = c.fetchall()

    close_db(conn, c)

    return render_template('friendships.html', trainers = trainers, friendships = friendships)

@friendship.route('/friendship/add')
def friendship_add():
    trainers = [int(request.args.get('trainer1')), int(request.args.get('trainer2'))]
    days = request.args.get('days')

    luck = 1
    if request.args.get('luck') is None:
        luck = 0

    if trainers[0] == trainers[1]:
        flash('Same Trainer!', 'error')

    else:
        if int(days):
            luck = 0

        conn, c = open_db('remote.db')

        sql = '''
        SELECT friendship_id
          FROM friendships
         WHERE trainer1_id == ? AND trainer2_id == ?
        '''
        c.execute(sql, (min(trainers), max(trainers)))

        if c.fetchone() is None:
            sql = '''
            INSERT INTO friendships (trainer1_id, trainer2_id, friendship_days, friendship_luck, friendship_last_interaction)
            VALUES (?, ?, ?, ?, ?)
            '''

            c.execute(sql, (min(trainers), max(trainers), days, luck, int(datetime.timestamp(datetime.now()))))
            conn.commit()

        close_db(conn, c)

        flash('Friendship created!', 'success')

    referrer = re.search(r'\/(trainer|tournament)\/friendships\/(\d+)$', request.referrer)

    if referrer is None:
        return redirect(url_for('.friendships'))

    else:
        if referrer.group(1) == 'trainer':
            return redirect(url_for('trainer.trainer_friendships', trainer_id = referrer.group(2)))

        else:
            return redirect(url_for('tournament.tournament_friendships', tournament_id = referrer.group(2)))

@friendship.route('/friendship/edit/<int:friendship_id>')
def friendship_edit(friendship_id):
    conn, c = open_db('remote.db')

    sql = '''
    SELECT friendships.friendship_id as friendship_id,
           trainers1.trainer_nick as trainer1_nick,
           trainers2.trainer_nick as trainer2_nick,
           friendships.friendship_days as friendship_days,
           friendships.friendship_luck as friendship_luck
      FROM friendships
           INNER JOIN trainers as trainers1
           ON trainers1.trainer_id = friendships.trainer1_id

           INNER JOIN trainers as trainers2
           ON trainers2.trainer_id = friendships.trainer2_id
     WHERE friendship_id = ?
    '''

    c.execute(sql, (friendship_id, ))

    friendship = c.fetchone()

    close_db(conn, c)

    if friendship is None:
        return redirect(url_for('.friendships'))

    else:
        return render_template('friendships_edit.html', friendship = friendship)

@friendship.route('/friendship/edit')
def friendship_edit_action():
    fid = request.args.get('id')
    days = request.args.get('days')

    luck = 1
    if request.args.get('luck') is None:
        luck = 0

    if int(days):
        luck = 0

    conn, c = open_db('remote.db')

    sql = '''
    UPDATE friendships
       SET friendship_days = ?,
           friendship_luck = ?
     WHERE friendship_id = ?
    '''

    c.execute(sql, (days, luck, fid))
    conn.commit()

    close_db(conn, c)

    flash('Friendship edited!', 'success')

    return redirect(url_for('.friendships'))

@friendship.route('/friendship/remove/<int:friendship_id>')
def friendship_remove(friendship_id):
    conn, c = open_db('remote.db')

    sql = '''
    DELETE FROM friendships
     WHERE friendship_id = ?
    '''

    c.execute(sql, (friendship_id, ))
    conn.commit()

    close_db(conn, c)

    flash('Friendship removed!', 'success')

    return redirect(url_for('.friendships'))