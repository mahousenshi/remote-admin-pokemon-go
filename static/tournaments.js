$(function() {
    $('form').on('submit', function(e) {
        let name = $('[name="name"]');
        let trainers = ;

        if (name.val().length < 4 || name.val().length > 15) {
            $('#messages').html('<p class="message error">Invalid tournament name (More than 3 less than 16)!</p>');
            name.focus();

            e.preventDefault();
        } else if ($('[type="checkbox"]:checked').length > 1) {
            $('#messages').html('<p class="message error">Too few trainers!</p>');

            e.preventDefault();
        }
    });
});