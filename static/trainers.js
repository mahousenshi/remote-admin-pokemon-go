$(function() {
    $('form').on('submit', function(e) {
        let nick = $('[name="nick"]');
        let code = $('[name="code"]');

        if (nick.val().length < 4 || nick.val().length > 15) {
            $('#messages').html('<p class="message error">Invalid nickname (More than 3 less than 16)!</p>');
            nick.focus();

            e.preventDefault();
        } else if (code.val().length != 12) {
            $('#messages').html('<p class="message error">Invalid code length!</p>');
            code.focus();

            e.preventDefault();
        }
    });

    $('[name="code"]').on('keypress', function(e) {
        e.preventDefault();

        if (parseInt(e.key, 10) + 1) {
            let val = $(this).val();

            if (val.length < 12) {
                $(this).val(val + e.key);
            }
        }
    });
});