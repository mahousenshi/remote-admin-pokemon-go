$(function() {
    $('td').on('click', function() {
        const elem = $(this);
        const level = elem.attr('class').split(' ');

        if (level.includes('egg')) {
            if (confirm("This action will level up a friendship and the trainer wants to use a egg. Continue?")) {
                elem.removeClass('egg').click();
                mirror(elem).removeClass('egg').click();
            }

        } else if (!level.includes('daily') && level.includes_array(['good', 'great', 'ultra', 'best'])) {
            $.getJSON('/_update',
            {
                id: elem.attr('id')
            },
            function (friendship) {
                console.log(friendship['friendship_level'])
                elem.html(friendship.friendship_note).removeClass().addClass(friendship.friendship_level);
                mirror(elem).html(friendship.friendship_note).removeClass().addClass(friendship.friendship_level);
            })

        } else if (level.includes('nofriendship')) {
            $.getJSON('/_new',
            {
                trainer1: elem.parent().attr('id'),
                trainer2: $('tr').eq(0).children('td').eq(elem.index()).attr('id')
            },
            function (friendship) {
                elem.removeClass().addClass('great daily').attr('id', friendship.friendship_id);
                mirror(elem).removeClass().addClass('great daily').attr('id', friendship.friendship_id);
            })
        };
    });
});

Array.prototype.includes_array = function (o_arr) {
    var arr = this;

    return !o_arr.every(function (e) {
        return !arr.includes(e)
    })
}

function mirror(elem) {
    return $('tr:eq(' + elem.index() + ') td:eq(' + elem.parent().index() + ')');
}