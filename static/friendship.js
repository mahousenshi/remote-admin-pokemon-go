$(function() {
    $('form').on('submit', function(e) {
        if ($('[name="trainer1"]').length && $('[name="trainer1"]').val() === $('[name="trainer2"]').val()) {
            if ($('[name="trainer1"]').val() === null) {
                $('#messages').html('<p class="message error">No Trainers!</p>')
            } else {
                $('#messages').html('<p class="message error">Same Trainer!</p>')
            }

            $('[name="trainer1"]').focus();
            e.preventDefault();
        }
    });

    $('[name="days"]').on('change', function(e) {
        if ($(this).val() == '0') {
            $('[name="luck"]').prop('disabled', false);
        } else {
            $('[name="luck"]').prop('disabled', true).prop('checked', false);
        }
    })
});