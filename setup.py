import sqlite3

def dict_factory(cursor, row):
    return {col[0]: row[idx] for idx, col in enumerate(cursor.description)}

conn = sqlite3.connect('remote.db')
conn.row_factory = dict_factory

c = conn.cursor()

sql = '''
SELECT name
    FROM sqlite_master
'''

c.execute(sql)

tables = [table['name'] for table in c.fetchall()]

if 'trainers' not in tables:
    sql = '''
    CREATE TABLE "trainers" (
        "trainer_id" INTEGER PRIMARY KEY AUTOINCREMENT,
        "trainer_nick"	TEXT,
        "trainer_code"	TEXT,
        "trainer_egg"	INTEGER
    )
    '''

    c.execute(sql)
    conn.commit()

if 'friendships' not in tables:
    sql = '''
    CREATE TABLE "friendships" (
        "friendship_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
        "trainer1_id" INTEGER,
        "trainer2_id" INTEGER,
        "friendship_days" INTEGER,
        "friendship_luck" INTEGER,
        "friendship_last_interaction" INTEGER
    )
    '''

    c.execute(sql)
    conn.commit()

if 'tournaments' not in tables:
    sql = '''
    CREATE TABLE "tournaments" (
        "tournament_id"	INTEGER PRIMARY KEY AUTOINCREMENT,
        "tournament_name"	TEXT,
        "tournament_URL"	TEXT,
        "tournament_is_closed"	INTEGER
    )
    '''

    c.execute(sql)
    conn.commit()

if 'tournaments_trainers' not in tables:
    sql = '''
    CREATE TABLE "tournaments_trainers" (
        "tournament_id"	INTEGER,
        "trainer_id"	INTEGER
    )
    '''

    c.execute(sql)
    conn.commit()

c.close()
conn.close()
