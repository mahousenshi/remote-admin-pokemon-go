from flask import Blueprint, render_template, request, redirect, url_for, flash
from datetime import datetime

import sqlite3

trainer = Blueprint('trainer', __name__)
trainer.secret_key = '36192dccc8c55f702233619b'

def dict_factory(cursor, row):
    return {col[0]: row[idx] for idx, col in enumerate(cursor.description)}

def open_db(name):
    conn = sqlite3.connect(name)
    conn.row_factory = dict_factory

    c = conn.cursor()

    return conn, c

def close_db(conn, cursor):
    cursor.close()
    conn.close()

# ======= #
# Trainer #
@trainer.route('/trainers/')
def trainers():
    conn, c = open_db('remote.db')

    sql = '''
    SELECT trainer_id, trainer_nick
      FROM trainers
    '''

    c.execute(sql)

    trainers = c.fetchall()

    close_db(conn, c)

    return render_template('trainers.html', trainers = trainers)

@trainer.route('/trainer/add')
def trainer_add():
    nick = request.args.get('nick')
    code = request.args.get('code')

    egg = 1
    if request.args.get('egg') is None:
        egg = 0

    if len(nick) < 4 or len(nick) > 15:
        flash('Invalid nickname (More than 3 less than 16)!', 'error')

    elif len(code) != 12:
        flash('Invalid code length!', 'error')

    elif not code.isnumeric():
        flash('Invalid code!', 'error')

    else:
        conn, c = open_db('remote.db')

        sql = '''
        SELECT trainer_id
          FROM trainers
         WHERE trainer_code = ?
        '''

        c.execute(sql, (code, ))

        if c.fetchone() is None:
            sql = '''
            INSERT INTO trainers(trainer_nick, trainer_code, trainer_egg)
            VALUES (?, ?, ?)
            '''

            c.execute(sql, (nick, code, egg))
            conn.commit()

            flash('Trainer created!', 'success')
        else:
            flash('Code already in use!', 'error')

        close_db(conn, c)

    return redirect(url_for('.trainers'))

@trainer.route('/trainer/edit/<int:trainer_id>')
def trainer_edit(trainer_id):
    conn, c = open_db('remote.db')

    sql = '''
    SELECT trainer_id, trainer_nick, trainer_code, trainer_egg
      FROM trainers
     WHERE trainer_id = ?
    '''

    c.execute(sql, (trainer_id, ))

    trainer = c.fetchone()

    close_db(conn, c)

    if trainer is None:
        flash('Invalid Trainer!', 'error')

        return redirect(url_for('.trainers'))
    else:
        return render_template('trainers_edit.html', trainer = trainer)

@trainer.route('/trainer/edit')
def trainer_edit_action():
    tid = request.args.get('id')
    nick = request.args.get('nick')
    code = request.args.get('code')

    egg = 1
    if request.args.get('egg') is None:
        egg = 0

    if len(nick) < 4 or len(nick) > 15:
        flash('Invalid nickname (More than 3 less than 16)!', 'error')

    elif len(code) != 12:
        flash('Invalid code length!', 'error')

    elif not code.isnumeric():
        flash('Invalid code!', 'error')

    else:
        conn, c = open_db('remote.db')

        sql = '''
        SELECT trainer_id
          FROM trainers
         WHERE trainer_code = ?
        '''

        c.execute(sql, (code, ))

        if c.fetchone() is None:
            sql = '''
            UPDATE trainers
               SET trainer_nick = ?,
                   trainer_code = ?,
                   trainer_egg = ?
             WHERE trainer_id = ?
            '''

            c.execute(sql, (nick, code, egg, tid))
            conn.commit()

            flash('Trainer edited!', 'success')

            return redirect(url_for('.trainers'))
        else:
            flash('Code already in use!', 'error')

            return redirect(url_for('.trainer_edit', trainer_id = tid))

        close_db(conn, c)

@trainer.route('/trainer/remove/<int:trainer_id>')
def trainer_remove(trainer_id):
    conn, c = open_db('remote.db')

    sql = '''
    DELETE FROM friendships
     WHERE trainer1_id = ?
        OR trainer2_id = ?
    '''

    c.execute(sql, (trainer_id, trainer_id))
    conn.commit()

    sql = '''
    DELETE FROM tournaments_trainers
     WHERE trainer_id = ?
    '''

    c.execute(sql, (trainer_id, ))
    conn.commit()

    sql = '''
    DELETE FROM trainers
     WHERE trainer_id = ?
    '''

    c.execute(sql, (trainer_id, ))
    conn.commit()

    close_db(conn, c)

    flash('Trainer removed!', 'success')

    return redirect(url_for('.trainers'))

@trainer.route('/trainer/friendships/<int:trainer_id>')
def trainer_friendships(trainer_id):
    conn, c = open_db('remote.db')

    sql = '''
    SELECT *
      FROM trainers
    '''

    c.execute(sql)

    trainers = c.fetchall()

    sql = '''
    SELECT *
      FROM friendships
     WHERE trainer1_id = ?
        OR trainer2_id = ?
     ORDER BY trainer1_id, trainer2_id
    '''

    c.execute(sql, (trainer_id, trainer_id))

    friendships_temp = c.fetchall()

    close_db(conn, c)

    friendships = {}

    for trainer in trainers:
        if trainer['trainer_id'] == trainer_id:
            this_trainer = trainer

        else:
            friendship = next((elem for elem in friendships_temp if elem['trainer1_id'] == trainer['trainer_id'] or elem['trainer2_id'] == trainer['trainer_id']), None)

            if friendship is None:
                friendship = {
                    'friendship_id': 0,
                    'friendship_level': 'nofriendship ',
                    'friendship_note': ' ',
                }

            else:
                if friendship['friendship_days'] < 1:
                    friendship['friendship_level'] = 'best'
                elif 1 <= friendship['friendship_days'] <= 30:
                    friendship['friendship_level'] = 'ultra'
                elif 30 < friendship['friendship_days'] <= 83:
                    friendship['friendship_level'] = 'great'
                else:
                    friendship['friendship_level'] = 'good'

                today = datetime.now()
                last_interaction = datetime.fromtimestamp(friendship['friendship_last_interaction'])

                if today.date() == last_interaction.date():
                    friendship['friendship_level'] += ' daily'

                friendship['friendship_note'] = ''

                if trainer['trainer_egg'] and friendship['friendship_days'] in (84, 30, 1):
                    friendship['friendship_level'] += ' egg'
                    friendship['friendship_note'] = '🥚'

                if friendship['friendship_luck']:
                    friendship['friendship_note'] = '🌟'

                if friendship['friendship_days'] >= 30:
                    delta = today - last_interaction

                    friendship['friendship_note'] += f'{"⚠️" * (delta.days // 5)}'

            friendships[trainer['trainer_id']] = friendship

    return render_template('trainers_friendships.html', trainers = trainers, this_trainer = this_trainer, friendships = friendships)