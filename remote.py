from flask import Flask, render_template

from trainer import trainer
from friendship import friendship
from friendship_table import friendship_table
from tournament import tournament

app = Flask(__name__)
app.secret_key = '00472072ed7aea4a6ea523ac'

app.register_blueprint(trainer)
app.register_blueprint(friendship)
app.register_blueprint(friendship_table)
app.register_blueprint(tournament)

# ===== #
# Index #
@app.route('/')
def main():
    return render_template('index.html')

if __name__ == '__main__':
    app.run()